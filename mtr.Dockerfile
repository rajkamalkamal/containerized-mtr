FROM alpine:latest

RUN apk update && apk add \
	mtr \
	&& rm -rf /var/cache/apk/*

ENTRYPOINT ["mtr"]

