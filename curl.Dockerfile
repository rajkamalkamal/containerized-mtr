FROM alpine:latest

# build initial cache | install binary | remove cache
RUN apk update && apk add \
	curl \
	&& rm -rf /var/cache/apk/*

ENTRYPOINT ["curl"]

