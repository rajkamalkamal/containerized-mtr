FROM alpine:latest

# build initial cache | install binary | remove cache
RUN apk update && apk add \
	fping \
	&& rm -rf /var/cache/apk/*

ENTRYPOINT ["fping"]

