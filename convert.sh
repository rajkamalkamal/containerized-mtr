pandoc README.md -s -o index.html -c github-markdown.css  --metadata title="Containerized mtr"

# REMINDER:
#
# After converting with the above, do the following:
#
# Import the github-markdown.css file and add a markdown-body class to the container of your rendered Markdown and set a width for it. GitHub uses 980px width and 45px padding, and 15px padding for mobile.


