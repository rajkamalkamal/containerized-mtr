FROM alpine:latest

# build initial cache | install binary | remove cache
RUN apk update && apk add \
  zsh \
  bash \
  curl \
  fping \
  mtr \
  && rm -rf /var/cache/apk/*

COPY mtr-loop.sh .

ENTRYPOINT ["/bin/bash", "mtr-loop.sh"]
